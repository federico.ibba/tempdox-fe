import React from 'react';
import { Box, Button } from '@material-ui/core/';
import { Link } from 'react-router-dom';

const Header = () => (
    <Box p={2} bgcolor="primary.main" display ="flex" justifyContent="space-between">
        <Link to = "/sign-in">
            <Button variant="contained">
                SIGN IN
            </Button>
        </Link>
        <Link to = "/sign-up">
            <Button variant="contained">
                SIGN UP
            </Button>
        </Link>
    </Box>
)

export default Header;