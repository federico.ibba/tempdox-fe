const siteUrl = process.env.REACT_APP_API_HOST;

export const signIn = ({username, password}) => (
    fetch(`${siteUrl}/api/login`, {
        method: 'POST',
        headers: new Headers({
            'Authorization': `Basic ${btoa(`${username}:${password}`)}`
        }),
    })
);
