import { setIn } from 'final-form';

const stringRequired = (str) => {
    if(!str || str === ''){
        return 'required'
    }
    return undefined;
};

const validateFinalWithYup = async ( values, validationSchema ) => {
	try {
		await validationSchema.validate( values, { abortEarly: false } );
	} catch ( err ) {
		const errors = err.inner.reduce( ( formError, innerError ) => {
			return setIn( formError, innerError.path, innerError.message );
		}, {} );

		return errors;
	}
};

export { 
	stringRequired,
	validateFinalWithYup,
};
