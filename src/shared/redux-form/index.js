import React from 'react';

import { TextField as MaterialTextField } from '@material-ui/core';

export const TextField = ({
	input: { name, onChange, value, ...restInput },
	meta,
	...rest
}) => (
	<MaterialTextField
		{...rest}
		name={name}
		helperText={meta.touched ? meta.error : undefined}
		error={!!meta.error && !!meta.touched}
		InputProps={restInput}
		onChange={onChange}
		value={value}
	/>
);
