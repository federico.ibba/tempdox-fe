import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { Dashboard, SignInContainer, SignUpContainer } from './features';
import { Footer, Header } from './components';
import Box from '@material-ui/core/Box';


import './App.css';

const App = () => (
  <BrowserRouter>
    <Box display="flex" height="100%" width ="100%" flexDirection = "column">
      <Header/>
      <Box display="flex" height="100%" width ="100%" flexDirection = "column" flex="1">
        <Switch>
          <Route path="/sign-in" component={SignInContainer}/>
          <Route path="/sign-up" component={SignUpContainer}/>
          <Route path="/" component={Dashboard} exact/>
          <Route path="**" component={() => <h2>Not Found</h2>} />
        </Switch>
      </Box>
      <Footer/>
    </Box>
  </BrowserRouter>
);

export default App;
