import {object, string }  from 'yup';

const validatorForm = object({
    username: string()
        .required('Username is required'),
    password: string()
        .required('Password is required')
})

export default validatorForm;