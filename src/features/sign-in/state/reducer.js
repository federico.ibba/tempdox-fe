

import {
    SIGN_IN_FETCH,
    SIGN_IN_RESPONSE,
    SIGN_IN_ERROR
} from './constants';

const initialState = {
    isLoading :false,
    isError :false,
    data: null,
    error: null,
}

const reducer = (state = initialState, action) => {
    switch (action.type){
        case SIGN_IN_FETCH:
            return {...state, isLoading: true}
        case SIGN_IN_RESPONSE:
            return {...state, isLoading: false, data: action.payload}
        case SIGN_IN_ERROR:
            return {...state, isLoading: false, isError: true, error: action.payload}
        default:
            return state
    }
}

export default reducer;