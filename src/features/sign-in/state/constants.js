const SIGN_IN_FETCH = 'app/signin/fetch';
const SIGN_IN_RESPONSE = 'app/signin/response';
const SIGN_IN_ERROR = 'app/signin/error';

export {
    SIGN_IN_FETCH,
    SIGN_IN_RESPONSE,
    SIGN_IN_ERROR
}