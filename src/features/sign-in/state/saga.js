import { call, takeLatest, put } from 'redux-saga/effects';
import { responseSignIn, errorSignIn } from './actions';
import { SIGN_IN_FETCH } from './constants';
import { signIn }  from '../../../api';

function* signInSaga({ payload, type }){
    try {
        const callDone = yield call(signIn, payload);
        const result = yield callDone.json();       

        yield put(responseSignIn(result));
        
    } catch (err) {
        yield put(errorSignIn(err))
    }

}

const sagas = [takeLatest(SIGN_IN_FETCH, signInSaga)];

export default sagas;