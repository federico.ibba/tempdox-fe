import {
    SIGN_IN_FETCH,
    SIGN_IN_RESPONSE,
    SIGN_IN_ERROR
} from './constants';

export function fetchSignIn(payload) {
    return {
      type: SIGN_IN_FETCH,
      payload,
    };
}

export function responseSignIn(payload) {
    return {
      type: SIGN_IN_RESPONSE,
      payload,
    };
}

export function errorSignIn(payload) {
    return {
      type: SIGN_IN_ERROR,
      payload,
    };
}