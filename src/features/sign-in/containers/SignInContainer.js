import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import Box from '@material-ui/core/Box';
import SignInForm from '../components/SignInForm';
import { fetchSignIn } from '../state/actions';
import { Selector } from '../state/selectors'

const SignInContainer = ({ signInData, makeSignIn }) => {

    useEffect(() => {
        if (signInData.data) {
            alert('Login done!');
        }
    }, [signInData])

    const onSubmit = (values) => {
        makeSignIn(values);
    }

    return(
    <Box 
        display="flex" 
        height="100%" 
        width ="100%" 
        flexDirection = "column" 
        flex="1" 
        alignItems= "center"
        justifyContent="center"
    >
        <SignInForm onSubmit = {onSubmit}/>
    </Box>
    )
}

const mapStateToProps = (state) => ({
    signInData: Selector.signIn(state)
})

const dispatchToProps = {
    makeSignIn: fetchSignIn
};

export default connect(mapStateToProps, dispatchToProps)(SignInContainer);
