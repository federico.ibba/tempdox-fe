import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextField } from '../../../shared/redux-form';
// import validatorForm from '../validator';
import { stringRequired } from '../../../shared/validator';

const SignInForm = ( { handleSubmit, pristine, reset, submitting }) => (
    <form onSubmit={handleSubmit}>
        <div>
        <div>
            <Field
                name="username"
                label="UserName"
                type="text"
                placeholder="User Name"
                component={TextField}
                validate = {stringRequired}
            />
        </div>
        </div>
        <div>
        <label>Password</label>
        <div>
            <Field
                name="password"
                type="password"
                placeholder="Password"
                component={TextField}
                validate = {stringRequired}
            />
        </div>
        </div>
        <div>
        <button type="submit" disabled={pristine || submitting}>
            Submit
        </button>
        </div>
    </form>
);

export default reduxForm({
    form: 'signInForm', // a unique identifier for this form
  })(SignInForm)