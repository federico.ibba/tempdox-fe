import React from 'react';
import { Box, Typography } from '@material-ui/core';

import FinalFormContainer from './FinalFormContainer';
// import FormikContainer from './FormikContainer';
// import { ReduxSignUpForm } from '../components';

const SignUpContainer = () => {
	const onSubmit = (values) => {
		console.log(values);
		alert(`Thanks ${values.username}!`)
	}

	return (
		<Box
			alignItems="center"
			display="flex" 
			flexDirection="column" 
			height="100%" 
			justifyContent="center"
			width ="100%" 
		>
			<Box
				border="1px solid lightgray"
				borderRadius="10px"
				display="flex"
				flexDirection="column"
				p={2}
				maxWidth="450px"
				width="100%"
				textAlign="center"
			>
				<Typography component="h3" variant="h5" style={{marginBottom: '16px'}}>
					Sign Up with TempDox
				</Typography>
				<FinalFormContainer onSubmit={onSubmit} />
			</Box>
		</Box>
	);
}

export default SignUpContainer;