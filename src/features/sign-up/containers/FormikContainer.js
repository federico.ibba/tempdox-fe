import React from 'react';
import { Formik } from 'formik';

import FormikSignUp from '../components';

import { initialValues } from '../map';
import { vanillaValidate } from '../validators';

/** 
 * To use the Yup validators with Formik follow these steps:
 * - import { yupValidators } from '../validators';
 * - Pass the validators directly to the Formik element using:
 *     ==> validationSchema={yupValidators}
**/

/** 
 * To use the Fonk validators with Formik follow these steps:
 * - import { formikFonkValidation } from '../validators';
 * - Uncomment the fonkValidation function and pass it to the Form using:
 *     ==> validationSchema={values => formikVanillaValidator(values)}
**/

/** 
 * To use the Vanilla validators with Formik follow these steps:
 * - import { vanillaValidate } from '../validators';
 * - Pass the validator function to Formik using:
 *     ==> validate={vanillaValidate}
**/

// const fonkValidation = (values) => formikFonkValidation.validateForm(values);

const FormikContainer = ({ onSubmit }) => (
    <Formik
        onSubmit={onSubmit}
        component={FormikSignUp}
        initialValues={initialValues}
        validate={vanillaValidate}
    />
);

export default FormikContainer;