import React from 'react';
import { Form } from 'react-final-form';

import { FinalSignUpForm } from '../components';

import { initialValues } from '../map';

import { yupValidators } from '../validators'
import { validateFinalWithYup } from '../../../shared/validator';

// import { finalFonkValidation } from '../validators';
// import { vanillaValidatorSchema } from '../validators/vanilla.validators';


/** 
 * To use the Yup validators with Final Form follow these steps:
 * - import { validateFinalWithYup } from '../../../shared/validator';
 * - import { yupValidators } from '../validators';
 * - Uncomment the formikValidation function and pass it to the Form
**/

/** 
 * To use the Fonk validators with Final Form follow these steps:
 * - import { finalFonkValidation } from '../validators';
 * - Uncomment the fonkValidation function and pass it to the Form
**/

/** 
 * To use the Vanilla validators with Formik follow these steps:
 * - import { vanillaValidate } from '../validators';
 * - Pass the validator function to Formik using:
 *     ==> validate={vanillaValidator} 
**/

const formikValidation = (values) => validateFinalWithYup(values, yupValidators);
// const fonkValidation = (values) => finalFonkValidation.validateForm(values);

const FinalFormContainer = ({ onSubmit }) => (
    <Form 
        component={FinalSignUpForm}
        initialValues={initialValues} // Not Required
        validate={formikValidation}
        onSubmit={onSubmit}
    />
);

export default FinalFormContainer;