import { createFinalFormValidation } from '@lemoncode/fonk-final-form';
import { createFormikValidation } from '@lemoncode/fonk-formik';

import yupValidators from './yup.validators';
import fonkSchema from './fonk.validators';
import { vanillaValidate, vanillaValidatorSchema } from './vanilla.validators';

const finalFonkValidation = createFinalFormValidation(fonkSchema);
const formikFonkValidation = createFormikValidation(fonkSchema);

export {
    yupValidators,
    vanillaValidate,
    vanillaValidatorSchema, 
    finalFonkValidation,
    formikFonkValidation,
};
