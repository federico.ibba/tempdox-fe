const isStringUndefined = (str) => !str || str === '';
const mailRegex = new RegExp(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i);
const passRegex = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/);

export const vanillaValidatorSchema = (values) => {
    const errors = {};

    if (isStringUndefined(values.name)) {
        errors.name = 'Name is required';
    }

    if (isStringUndefined(values.surname)) {
        errors.surname = 'Surname is required';
    }

    if (isStringUndefined(values.username)) {
        errors.username = 'Username is required';
    }

    if (isStringUndefined(values.email)) {
        errors.email = 'Email is required';
    } else if (!mailRegex.test(values.email)) {
        errors.email = 'Please type a valid email';
    }

    if (isStringUndefined(values.password)) {
        errors.password = 'Password is required';
    } else if (!passRegex.test(values.password)) {
        errors.password = 'The password must match: minimum eight characters, at least one letter and one number';
    }

    if (isStringUndefined(values.repeatPassword)) {
        errors.repeatPassword = 'Repeat password is required';
    } else if (values.password !== values.repeatPassword) {
        errors.repeatPassword = 'Passwords must match';
    }
    
    return errors;
};

export const vanillaValidate = (values) => vanillaValidatorSchema(values);
