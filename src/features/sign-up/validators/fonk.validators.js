import { Validators } from '@lemoncode/fonk';
import { matchField } from '@lemoncode/fonk-match-field-validator';

const validationSchema = {
    field: {
        name: [
            {
                validator: Validators.required.validator,
                message: 'The name is required'
            }
        ],
        surname: [
            {
                validator: Validators.required.validator,
                message: 'The surname is required'
            }
        ],
        username: [
            {
                validator: Validators.required.validator,
                message: 'The username is required'
            }
        ],
        email: [
            {
                validator: Validators.required.validator,
                message: 'The email is required'
            }, {
                validator: Validators.email.validator,
                message: 'Please type a valid email'
            }
        ],
        password: [
            {
                validator: Validators.required.validator,
                message: 'The password is required'
            }, {
                validator: Validators.pattern.validator,
                customArgs: { pattern: new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/) },
                message: 'The password must match: minimum eight characters, at least one letter and one number'
            }
        ],
        repeatPassword: [
            {
                validator: Validators.required.validator,
                message: 'Repeat password is required'
            }, {
                validator: matchField.validator,
                customArgs: { field: 'password' },
                message: 'Passwords must match'
            }
        ]        
    }
};

export default validationSchema;