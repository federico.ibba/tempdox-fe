import FormikSignUp from './FormikSignUp';
import ReduxSignUpForm from './ReduxSignUpForm';
import FinalSignUpForm from './FinalSignUpForm';

export {
    FormikSignUp,
    ReduxSignUpForm,
    FinalSignUpForm
}