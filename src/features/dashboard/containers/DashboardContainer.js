import React from 'react';

import { Dashboard } from '../components'

const DashboardContainer = () => (
    <div>
        <h2>Dashboard</h2>
        <Dashboard username="Gino" />
    </div>
);

export default DashboardContainer;
