import { all } from 'redux-saga/effects';
import SignInSagas from '../features/sign-in/state/saga';

function* rootSaga() {
    yield all([
        ...SignInSagas
    ]);
}

export default rootSaga;
