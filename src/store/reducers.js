import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import SignInReducer from '../features/sign-in/state/reducer';


const rootReducer = combineReducers({
    form: reduxFormReducer,
    signIn: SignInReducer,
});

export default rootReducer;
